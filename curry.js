// Currys two functions together and applies arguments when present.
function curryIt(f) {
  return function(a) {
    return function(b) {
      return f(a, b);
    }
  }
}
// Sum up two operands.
function sumItUp(a, b) {
  return a + b;
}

let spices = curryIt(sumItUp)
// Call sumItUp by currying arguments 5 and 6.
console.log(spices(5)(6));
